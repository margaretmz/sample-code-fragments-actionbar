package edu.uw.android210.lecture6.example;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * @author margaret
 * February 22, 2014
 * Sample Code that demonstrates the concept of fragment & actionbar 
 */
public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Set the Home/Up button as disabled, since there is no hierarchical parent.
        actionBar.setHomeButtonEnabled(false);
		
		Button study_btn = (Button)findViewById(R.id.button1);
		Button testenglish_btn = (Button)findViewById(R.id.button2);
		Button testchinese_btn = (Button)findViewById(R.id.button3);
		
		study_btn.setOnClickListener(this);
		testenglish_btn.setOnClickListener(this);
		testchinese_btn.setOnClickListener(this);
		
		// add word of day fragment by code
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		WordofDayFragment fragment = new WordofDayFragment();
		fragmentTransaction.add(R.id.fragment_container, fragment);
		fragmentTransaction.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * define actions for actionbar icons
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		Intent i; 
    	
    	switch (item.getItemId()) {    	
    		case R.id.action_about:
    			i = new Intent(this, AboutActivity.class);
    			startActivity(i);
    			break;
    	}
		
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * onClick for each button
	 */
	@Override
	public void onClick(View v) {
		Intent i = new Intent();
		
		switch(v.getId()){
			case R.id.button1:
				i.setClass(this, StudyActivity.class);
				startActivity(i);
				break;
			case R.id.button2:
				// do something here
				i.setClass(this, TestActivity.class);
				startActivity(i);
				break;
			case R.id.button3:
				// do something here
				Toast.makeText(this, "still working on it", Toast.LENGTH_SHORT).show();
				break;			
				
		}
		
	}

}
