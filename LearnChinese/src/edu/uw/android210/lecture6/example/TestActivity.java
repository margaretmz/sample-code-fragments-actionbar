package edu.uw.android210.lecture6.example;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * 
 * This is where you take the language tests
 *
 */
public class TestActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);

		// The content view embeds the answer key as a fragment
		// now retrieve the fragment and attach their "hide" button.
		FragmentManager fm = getFragmentManager();
		addShowHideListener(R.id.frag1hide, fm.findFragmentById(R.id.fragment_answerkey));
	}

	void addShowHideListener(int buttonId, final Fragment fragment) {
		final Button button = (Button) findViewById(buttonId);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.setCustomAnimations(android.R.animator.fade_in,
						android.R.animator.fade_out);
				if (fragment.isHidden()) {
					ft.show(fragment);
					button.setText("Hide Answer Key");
				} else {
					ft.hide(fragment);
					button.setText("Show Answer Key");
				}
				ft.commit();
			}
		});
	}
	/**
	 * a fragment that shows the answer key on @link TestActivity
	 *
	 */
	public static class AnswerKeyFragment extends Fragment {
		TextView mTextView;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_answerkey, container,
					false);
			
		}


	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test, menu);
		return true;
	}

}
