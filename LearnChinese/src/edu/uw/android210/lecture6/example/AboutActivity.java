package edu.uw.android210.lecture6.example;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
        // Set up the action bar.
		ActionBar actionBar = getActionBar();
		
		// navigate up with the app icon
	    actionBar.setDisplayHomeAsUpEnabled(true);


	}

}
